﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Ambiente {
    private long id;
    private string nombre;
    private string codigo;
    private int aforo;
    private string idVuforia;
    private string urlImagen;
    private int nroPiso;
    private Edificio edificio;
    [SerializeField] private List<AmbienteCurso> esUsadoCursos;
    [SerializeField] private List<EventoAmbiente> esUsadoEventos;
    [SerializeField] private List<TramiteAdministrativo> realizaTramites;
    [SerializeField] private List<PersonalAdministrativo> gestionaPersonal;
    [SerializeField] private TipoAmbiente tipoAmbiente;

    public List<EventoAmbiente> getEsUsadoEventos () {
        return this.esUsadoEventos;
    }

    public void setEsUsadoEventos (List<EventoAmbiente> esUsadoEventos) {
        this.esUsadoEventos = esUsadoEventos;
    }

    public long getId () {
        return this.id;
    }

    public void setId (long id) {
        this.id = id;
    }

    public int getNroPiso () {
        return this.nroPiso;
    }

    public void setNroPiso (int nroPiso) {
        this.nroPiso = nroPiso;
    }

    public string getNombre () {
        return this.nombre;
    }

    public void setNombre (string nombre) {
        this.nombre = nombre;
    }

    public Edificio getEdificio () {
        return this.edificio;
    }

    public void setEdificio (Edificio edificio) {
        this.edificio = edificio;
    }

    public string getCodigo () {
        return this.codigo;
    }

    public void setCodigo (string codigo) {
        this.codigo = codigo;
    }

    public int getAforo () {
        return this.aforo;
    }

    public void setAforo (int aforo) {
        this.aforo = aforo;
    }

    public string getIdvuforia () {
        return this.idVuforia;
    }

    public void setIdvuforia (string idVuforia) {
        this.idVuforia = idVuforia;
    }

    public string getUrlimagen () {
        return this.urlImagen;
    }

    public void setUrlimagen (string urlImagen) {
        this.urlImagen = urlImagen;
    }

    public List<AmbienteCurso> getEsusadocursos () {
        return this.esUsadoCursos;
    }

    public void setEsusadocursos (List<AmbienteCurso> esUsadoCursos) {
        this.esUsadoCursos = esUsadoCursos;
    }

    public List<TramiteAdministrativo> getRealizatramites () {
        return this.realizaTramites;
    }

    public void setRealizatramites (List<TramiteAdministrativo> realizaTramites) {
        this.realizaTramites = realizaTramites;
    }

    public List<PersonalAdministrativo> getGestionapersonal () {
        return this.gestionaPersonal;
    }

    public void setGestionapersonal (List<PersonalAdministrativo> gestionaPersonal) {
        this.gestionaPersonal = gestionaPersonal;
    }

    public TipoAmbiente getTipoambiente () {
        return this.tipoAmbiente;
    }

    public void setTipoambiente (TipoAmbiente tipoAmbiente) {
        this.tipoAmbiente = tipoAmbiente;
    }

}