﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Edificio
{
    private long id;
    private string nombre;
    private int nroPisos;
    private List<Ambiente> contieneAmbientes;

	public long getId()
	{
		return this.id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public string getNombre()
	{
		return this.nombre;
	}

	public void setNombre(string nombre)
	{
		this.nombre = nombre;
	}

	public int getNropisos()
	{
		return this.nroPisos;
	}

	public void setNropisos(int nroPisos)
	{
		this.nroPisos = nroPisos;
	}

	public List<Ambiente> getContieneambientes()
	{
		return this.contieneAmbientes;
	}

	public void setContieneambientes(List<Ambiente> contieneAmbientes)
	{
		this.contieneAmbientes = contieneAmbientes;
	}

}
