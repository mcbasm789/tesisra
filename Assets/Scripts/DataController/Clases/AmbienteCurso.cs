﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AmbienteCurso
{
    private long id;
    [SerializeField] private Ambiente ambiente;
    [SerializeField] private Curso curso;
    [SerializeField] private List<Horario> horarios;

    public long getId()
    {
        return this.id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public Ambiente getAmbiente()
    {
        return this.ambiente;
    }

    public void setAmbiente(Ambiente ambiente)
    {
        this.ambiente = ambiente;
    }

    public Curso getCurso()
    {
        return this.curso;
    }

    public void setCurso(Curso curso)
    {
        this.curso = curso;
    }

    public List<Horario> getHorarios()
    {
        return this.horarios;
    }

    public void setHorarios(List<Horario> horarios)
    {
        this.horarios = horarios;
    }

}
