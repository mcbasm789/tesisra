﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class EventoAmbiente
{
    private long id;
    private List<Horario> horarios;
    private Ambiente ambiente;
    private Evento evento;

    public long getId()
    {
        return this.id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public List<Horario> getHorarios()
    {
        return this.horarios;
    }

    public void setHorarios(List<Horario> horarios)
    {
        this.horarios = horarios;
    }

    public Ambiente getAmbiente()
    {
        return this.ambiente;
    }

    public void setAmbiente(Ambiente ambiente)
    {
        this.ambiente = ambiente;
    }

    public Evento getEvento()
    {
        return this.evento;
    }

    public void setEvento(Evento evento)
    {
        this.evento = evento;
    }

}
