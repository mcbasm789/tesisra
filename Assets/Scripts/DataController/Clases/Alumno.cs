﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Alumno
{
    private long id;
    private string apPaterno;
    private string apMaterno;
    private string nombre;
    private string codigo;
    private string estado;
    private List<Curso> llevaCursos;

    public long getId()
    {
        return this.id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public string getAppaterno()
    {
        return this.apPaterno;
    }

    public void setAppaterno(string apPaterno)
    {
        this.apPaterno = apPaterno;
    }

    public string getApmaterno()
    {
        return this.apMaterno;
    }

    public void setApmaterno(string apMaterno)
    {
        this.apMaterno = apMaterno;
    }

    public string getNombre()
    {
        return this.nombre;
    }

    public void setNombre(string nombre)
    {
        this.nombre = nombre;
    }

    public string getCodigo()
    {
        return this.codigo;
    }

    public void setCodigo(string codigo)
    {
        this.codigo = codigo;
    }

    public string getEstado()
    {
        return this.estado;
    }

    public void setEstado(string estado)
    {
        this.estado = estado;
    }

    public List<Curso> getLlevacursos()
    {
        return this.llevaCursos;
    }

    public void setLlevacursos(List<Curso> llevaCursos)
    {
        this.llevaCursos = llevaCursos;
    }


}
