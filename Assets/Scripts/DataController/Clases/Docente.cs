﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Docente 
{
    private long id;
    private string apPaterno;
    private string apMaterno;
    private string nombre;
    private List<Curso> dictaCursos;

	public long getId()
	{
		return this.id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public string getAppaterno()
	{
		return this.apPaterno;
	}

	public void setAppaterno(string apPaterno)
	{
		this.apPaterno = apPaterno;
	}

	public string getApmaterno()
	{
		return this.apMaterno;
	}

	public void setApmaterno(string apMaterno)
	{
		this.apMaterno = apMaterno;
	}

	public string getNombre()
	{
		return this.nombre;
	}

	public void setNombre(string nombre)
	{
		this.nombre = nombre;
	}

	public List<Curso> getDictacursos()
	{
		return this.dictaCursos;
	}

	public void setDictacursos(List<Curso> dictaCursos)
	{
		this.dictaCursos = dictaCursos;
	}

}
