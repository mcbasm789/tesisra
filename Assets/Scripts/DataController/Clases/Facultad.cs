﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Facultad
{
    private long id;
    private string nombre;
    private List<EscuelaProfesional> gestionaEscuelas;

    public long getId()
    {
        return this.id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public string getNombre()
    {
        return this.nombre;
    }

    public void setNombre(string nombre)
    {
        this.nombre = nombre;
    }

    public List<EscuelaProfesional> getGestionaescuelas()
    {
        return this.gestionaEscuelas;
    }

    public void setGestionaescuelas(List<EscuelaProfesional> gestionaEscuelas)
    {
        this.gestionaEscuelas = gestionaEscuelas;
    }

}
