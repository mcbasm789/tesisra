﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class TramiteAdministrativo
{
    private long id;
    private string nombre;
    private string descripcion;
    private string codigo;
    private List<Ambiente> esRealizadaOficinas;

    public long getId()
    {
        return this.id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public string getNombre()
    {
        return this.nombre;
    }

    public void setNombre(string nombre)
    {
        this.nombre = nombre;
    }

    public string getDescripcion()
    {
        return this.descripcion;
    }

    public void setDescripcion(string descripcion)
    {
        this.descripcion = descripcion;
    }

    public string getCodigo()
    {
        return this.codigo;
    }

    public void setCodigo(string codigo)
    {
        this.codigo = codigo;
    }

    public List<Ambiente> getEsrealizadaoficinas()
    {
        return this.esRealizadaOficinas;
    }

    public void setEsrealizadaoficinas(List<Ambiente> esRealizadaOficinas)
    {
        this.esRealizadaOficinas = esRealizadaOficinas;
    }

}

