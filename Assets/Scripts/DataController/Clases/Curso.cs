﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Curso
{
    private long id;
    private string nombre;
    private string codigo;
    private List<Alumno> esLlevadoAlumnos;
    private List<Docente> esDictadoDocentes;
    private List<AmbienteCurso> esDictadoAmbientes;
    private EscuelaProfesional escuelaProfesional;

    public long getId()
    {
        return this.id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public string getNombre()
    {
        return this.nombre;
    }

    public void setNombre(string nombre)
    {
        this.nombre = nombre;
    }

    public string getCodigo()
    {
        return this.codigo;
    }

    public void setCodigo(string codigo)
    {
        this.codigo = codigo;
    }

    public List<Alumno> getEsllevadoalumnos()
    {
        return this.esLlevadoAlumnos;
    }

    public void setEsllevadoalumnos(List<Alumno> esLlevadoAlumnos)
    {
        this.esLlevadoAlumnos = esLlevadoAlumnos;
    }

    public List<Docente> getEsdictadodocentes()
    {
        return this.esDictadoDocentes;
    }

    public void setEsdictadodocentes(List<Docente> esDictadoDocentes)
    {
        this.esDictadoDocentes = esDictadoDocentes;
    }

    public List<AmbienteCurso> getEsdictadoambientes()
    {
        return this.esDictadoAmbientes;
    }

    public void setEsdictadoambientes(List<AmbienteCurso> esDictadoAmbientes)
    {
        this.esDictadoAmbientes = esDictadoAmbientes;
    }

    public EscuelaProfesional getEscuelaprofesional()
    {
        return this.escuelaProfesional;
    }

    public void setEscuelaprofesional(EscuelaProfesional escuelaProfesional)
    {
        this.escuelaProfesional = escuelaProfesional;
    }

}
