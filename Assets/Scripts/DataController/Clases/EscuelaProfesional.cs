﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class EscuelaProfesional
{
    private long id;
    private string nombre;
    private Facultad facultad;
    private List<Curso> gestionaCursos;

    public long getId()
    {
        return this.id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public string getNombre()
    {
        return this.nombre;
    }

    public void setNombre(string nombre)
    {
        this.nombre = nombre;
    }

    public Facultad getFacultad()
    {
        return this.facultad;
    }

    public void setFacultad(Facultad facultad)
    {
        this.facultad = facultad;
    }

    public List<Curso> getGestionacursos()
    {
        return this.gestionaCursos;
    }

    public void setGestionacursos(List<Curso> gestionaCursos)
    {
        this.gestionaCursos = gestionaCursos;
    }

}
