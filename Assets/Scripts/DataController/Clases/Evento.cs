﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;

[System.Serializable]
public class Evento {
    private long id;
    private string nombre;
    private int aforo;
    private string expositor;
    private bool realizado;
    private DateTime fecha;
    private List<EventoAmbiente> esRealizadoAmbientes;

    public string getExpositor () {
        return this.expositor;
    }

    public void setExpositor (string expositor) {
        this.expositor = expositor;
    }

    public long getId () {
        return this.id;
    }

    public void setId (long id) {
        this.id = id;
    }

    public bool getRealizado () {
        return this.realizado;
    }

    public void isRealizado (bool realizado) {
        this.realizado = realizado;
    }

    public string getNombre () {
        return this.nombre;
    }

    public void setNombre (string nombre) {
        this.nombre = nombre;
    }

    public int getAforo () {
        return this.aforo;
    }

    public void setAforo (int aforo) {
        this.aforo = aforo;
    }

    public DateTime getFecha () {
        return this.fecha;
    }

    public void setFecha (DateTime fecha) {
        this.fecha = fecha;
    }

    public List<EventoAmbiente> getEsRealizadoAmbientes () {
        return this.esRealizadoAmbientes;
    }

    public void setEsRealizadoAmbientes (List<EventoAmbiente> esRealizadoAmbientes) {
        this.esRealizadoAmbientes = esRealizadoAmbientes;
    }

}