﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Cargo
{
    private long id;
    private string nombre;
    private List<PersonalAdministrativo> designaPersonal;

	public long getId()
	{
		return this.id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public string getNombre()
	{
		return this.nombre;
	}

	public void setNombre(string nombre)
	{
		this.nombre = nombre;
	}

	public List<PersonalAdministrativo> getDesignapersonal()
	{
		return this.designaPersonal;
	}

	public void setDesignapersonal(List<PersonalAdministrativo> designaPersonal)
	{
		this.designaPersonal = designaPersonal;
	}

}
