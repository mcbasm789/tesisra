﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Horario
{
    private long id;
    private string horaInicio;
    private string horaFin;
    private string dia;
    private int nroDia;

    private List<AmbienteCurso> cursos;
    private List<EventoAmbiente> eventos;

    public List<EventoAmbiente> getEventos()
    {
        return this.eventos;
    }

    public void setEventos(List<EventoAmbiente> eventos)
    {
        this.eventos = eventos;
    }

    public long getId()
    {
        return this.id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public string getHorainicio()
    {
        return this.horaInicio;
    }

    public void setHorainicio(string horaInicio)
    {
        this.horaInicio = horaInicio;
    }

    public string getHorafin()
    {
        return this.horaFin;
    }

    public void setHorafin(string horaFin)
    {
        this.horaFin = horaFin;
    }

    public string getDia()
    {
        return this.dia;
    }

    public void setDia(string dia)
    {
        this.dia = dia;
    }

    public int getNroDia()
    {
        return this.nroDia;
    }

    public void setNroDia(int nroDia)
    {
        this.nroDia = nroDia;
    }

    public List<AmbienteCurso> getCursos()
    {
        return this.cursos;
    }

    public void setCursos(List<AmbienteCurso> cursos)
    {
        this.cursos = cursos;
    }

}
