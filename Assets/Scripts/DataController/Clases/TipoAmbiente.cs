﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TipoAmbiente
{
    private long id;
    private string nombre;
    private List<Ambiente> registraAmbientes;

    public long getId()
    {
        return this.id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public string getNombre()
    {
        return this.nombre;
    }

    public void setNombre(string nombre)
    {
        this.nombre = nombre;
    }

    public List<Ambiente> getRegistraambientes()
    {
        return this.registraAmbientes;
    }

    public void setRegistraambientes(List<Ambiente> registraAmbientes)
    {
        this.registraAmbientes = registraAmbientes;
    }

}
