﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;
using Vuforia;

public class DisplayController : MonoBehaviour {
    #region Public_Variables
    //Botones
    public GameObject btnLimpiarPantalla;
    //Scroll Panels
    public GameObject centralScrollSnap;
    public GameObject cursosScrollSnap;
    public GameObject eventosScrollSnap;
    //Paginador central
    public Transform paginadorCentral;
    //Toggle Groups
    public GameObject centralPaginatorToggleGroup;
    public GameObject cursosPaginatorToggleGroup;
    public GameObject eventosPaginatorToggleGroup;
    //Scroll Snaps
    public GameObject eventosPaginatorScrollSnap;
    public GameObject cursosPaginatorScrollSnap;
    #endregion
    #region Private_Variables
    private DefaultTrackableEventHandler defaultTrackableEventHandler;
    //Horizontal scroll snap central
    private HorizontalScrollSnap hss;
    private VerticalScrollSnap vssCursos;
    private VerticalScrollSnap vssEventos;
    private bool estaLimpio = true;
    #endregion
    // Start is called before the first frame update
    void Start () {
        //Obtener el objeto dataController
        defaultTrackableEventHandler = FindObjectOfType<DefaultTrackableEventHandler> ();
        //Obtener el componente scroll snap del objeto
        hss = centralScrollSnap.GetComponent<HorizontalScrollSnap> ();
        vssCursos = cursosScrollSnap.GetComponent<VerticalScrollSnap> ();
        vssEventos = eventosScrollSnap.GetComponent<VerticalScrollSnap> ();
    }

    // Update is called once per frame
    void Update () { }

    //GEtter and setter
    public void setEstaLimpio (bool estaLimpio) {
        this.estaLimpio = estaLimpio;
    }

    public bool getEstaLimpio () {
        return this.estaLimpio;
    }

    #region Scroll_Methods
    public void cambioPaginaCentralScroll () {
        //Obtener el toggle group asociado
        ToggleGroup tg = centralPaginatorToggleGroup.GetComponent<ToggleGroup> ();
        //Obtener todos sus toogles
        IList<Toggle> listaToggles = ToggleGroupExtensions.GetToggles (tg);
        foreach (Toggle toggle in listaToggles) {
            //Obtener el componenete PaginatorToggle
            PaginatorToggle pgt = toggle.GetComponent<PaginatorToggle> ();
            if (pgt.getOrden () == hss.CurrentPage) {
                toggle.isOn = true;
                break;
            }
        }

    }

    public void cambioPaginaCursosScroll () {
        //Obtener el toggle group asociado
        ToggleGroup tg = cursosPaginatorToggleGroup.GetComponent<ToggleGroup> ();
        //Obtener el scroll snap asociado
        ScrollSnap ss = cursosPaginatorScrollSnap.GetComponent<ScrollSnap> ();
        //Obtener todos sus toogles
        IList<Toggle> listaToggles = ToggleGroupExtensions.GetToggles (tg);
        foreach (Toggle toggle in listaToggles) {
            //Obtener el componenete PaginatorToggle
            PaginatorToggle pgt = toggle.GetComponent<PaginatorToggle> ();
            if (pgt.getOrden () == vssCursos.CurrentPage) {
                toggle.isOn = true;
                ss.ChangePage (pgt.getOrden ());
                break;
            }
        }

    }

    public void cambioPaginaEventosScroll () {
        //Obtener el toggle group asociado
        ToggleGroup tg = eventosPaginatorToggleGroup.GetComponent<ToggleGroup> ();
        //Obtener el scroll snap asociado
        ScrollSnap ss = eventosPaginatorScrollSnap.GetComponent<ScrollSnap> ();
        //Obtener todos sus toogles
        IList<Toggle> listaToggles = ToggleGroupExtensions.GetToggles (tg);
        foreach (Toggle toggle in listaToggles) {
            //Obtener el componenete PaginatorToggle
            PaginatorToggle pgt = toggle.GetComponent<PaginatorToggle> ();
            if (pgt.getOrden () == vssEventos.CurrentPage) {
                toggle.isOn = true;
                ss.ChangePage (pgt.getOrden ());
                break;
            }
        }

    }
    #endregion    

    #region Button_Methods
    //Regresar a la pantalla de inicio
    public void RegresarInicio () {
        SceneManager.LoadScene ("Start", LoadSceneMode.Single);
    }

    public void IrEncuesta () {
        Application.OpenURL ("http://bit.ly/2GG8YiH");
    }
    #endregion    

    #region Clear_Methods
    public void limpiarPantalla () {
        //Limpiar los paginadores
        if (paginadorCentral.childCount > 0)
            destroyChildrensTransform (paginadorCentral);
        defaultTrackableEventHandler.OnTrackableStateChanged (TrackableBehaviour.Status.TRACKED, TrackableBehaviour.Status.NO_POSE);
        btnLimpiarPantalla.SetActive (false);
        estaLimpio = true;
    }

    private void destroyChildrensTransform (Transform tr) {
        foreach (Transform child in tr) {
            GameObject.Destroy (child.gameObject);
        }
    }
    #endregion
}