﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;
using Vuforia;

public class MainController : MonoBehaviour {
    #region Public_Variables
    public Text codigoAmbienteText;
    public Text nombreAmbienteText;
    public Text edificioText;
    public Text pisoText;
    public Text aforoText;
    public Text tipoAmbienteText;
    //Objetos de los Iconos
    public GameObject iconoDerecha;
    public GameObject iconoIzquierda;
    //Texturas de los Iconos
    public Sprite spriteAula;
    public Sprite spriteOficina;
    public Sprite spriteAuditorio;
    //Activar o desactivar un panel
    public GameObject dataBaseDisplay;
    public GameObject cursosDisplay;
    public GameObject tramitesDisplay;
    public GameObject personalDisplay;
    public GameObject eventosDisplay;
    public GameObject navigatorDisplay;
    //Activar o desactivar un Objecto
    public GameObject btnLimpiarPantalla;
    //Pool de objetos
    public SimpleObjectPool cursosPanelPool;
    public SimpleObjectPool tramitesPanelPool;
    public SimpleObjectPool personalPanelPool;
    public SimpleObjectPool eventosPanelPool;
    //Paneles padre para los pool de objetos
    public GameObject scrollSnapCursos;
    public Transform panelParentTramites;
    public Transform panelParentPersonal;
    public GameObject scrollSnapEventos;
    //Paneles dentro de los scroll snap
    public GameObject scrollContentCursos;
    //Scroll Panel de los paginadores
    public Transform paginadorCursos;
    public Transform paginadorEventos;
    public Transform paginadorCentral;
    //Panel central del scroll panel
    public Transform panelScroll;
    //Scroll panel
    public GameObject centralScrollSnap;

    //Prefabs a instanciar
    public GameObject prefabPaginatorToggle;
    //Variables adicionales para mostrar cursos
    public SimpleObjectPool alumnosPanelPool;
    public Transform alumnosPanelParent;
    public GameObject alumnosDisplay;
    #endregion

    #region Private_Variables
    //Listas de objetos para los paneles
    private List<GameObject> cursosPanelList = new List<GameObject> ();
    private List<GameObject> tramitesPanelList = new List<GameObject> ();
    private List<GameObject> personalPanelList = new List<GameObject> ();
    private List<GameObject> eventosPanelList = new List<GameObject> ();
    //Lista de objetos para los paneles paginadores
    private List<GameObject> paginadorCursosList = new List<GameObject> ();
    private List<GameObject> paginadorEventosList = new List<GameObject> ();
    private DefaultTrackableEventHandler defaultTrackableEventHandler;

    /* HttpClient para la consulta a la API */
    private HttpClient httpClient = new HttpClient ();
    /* URL Base de la API */
    private string urlBase = "http://167.86.91.74:8080/api";
    //Ambiente buscado
    private Ambiente ambienteBuscado;
    private string lastMarkerName;

    private bool firstScan;
    #endregion

    #region Default_Methods
    // Start is called before the first frame update
    void Start () {
        //Obtener el objeto dataController
        defaultTrackableEventHandler = FindObjectOfType<DefaultTrackableEventHandler> ();
        firstScan = true;
    }

    // Update is called once per frame
    async void Update () {
        // Get the Vuforia StateManager
        StateManager sm = TrackerManager.Instance.GetStateManager ();

        // Query the StateManager to retrieve the list of
        // currently 'active' trackables
        //(i.e. the ones currently being tracked by Vuforia)
        IEnumerable<TrackableBehaviour> activeTrackables = sm.GetActiveTrackableBehaviours ();

        bool estaActivo = true;
        // Iterate through the list of active trackables
        foreach (TrackableBehaviour tb in activeTrackables) {
            if (estaActivo) {
                //Verificar que no repita el marcador o si es el primer escaneo
                if (lastMarkerName != tb.TrackableName || firstScan) {
                    //Limpiar los pools
                    cleanPools ();
                    //Detener el scaneo mientras dura el proceso
                    TrackerManager.Instance.GetTracker<ObjectTracker> ().Stop ();

                    //Verificar el Tipo de Marcador
                    string tipoMarcador = tb.TrackableName.Split ('_') [0];

                    switch (tipoMarcador) {
                        case "Ambiente":
                            //Cargar los datos
                            ambienteBuscado = await GetAmbienteByIDVuforiaAsync (tb.TrackableName);
                            cargarDatos ();
                            //Reiniciar el scroll
                            HorizontalScrollSnap hss = centralScrollSnap.GetComponent<HorizontalScrollSnap> ();
                            hss.ChangePage (0);
                            //Mantenerlo en pantalla
                            defaultTrackableEventHandler.OnTrackableStateChanged (TrackableBehaviour.Status.NO_POSE, TrackableBehaviour.Status.TRACKED);
                            lastMarkerName = tb.TrackableName;
                            //Cancelar los metodos de carga por 2 segundos
                            estaActivo = false;
                            //Evitar que siga escaneando una vez haya encontrado el objeto
                            await Task.Factory.StartNew (() => {
                                System.Threading.Thread.Sleep (2000);
                                estaActivo = true;
                                TrackerManager.Instance.GetTracker<ObjectTracker> ().Start ();
                            });
                            break;
                    }
                    //El primer escaneo ya paso
                    if (firstScan)
                        firstScan = false;
                }
            }
        }
    }
    #endregion

    #region Scan_Methods
    private void cargarDatos () {
        //Asiginar los valores a los campos de texto
        if (ambienteBuscado.getCodigo () != null) {
            codigoAmbienteText.text = ambienteBuscado.getCodigo ();
            nombreAmbienteText.text = ambienteBuscado.getNombre ();
            edificioText.text = ambienteBuscado.getEdificio ().getNombre ();
            aforoText.text = ambienteBuscado.getAforo ().ToString ();
            tipoAmbienteText.text = ambienteBuscado.getTipoambiente ().getNombre ();
            pisoText.text = ambienteBuscado.getNroPiso ().ToString ();

            GameObject[] previousChildren = new GameObject[0];
            //Limpiar el scroll
            HorizontalScrollSnap hss = centralScrollSnap.GetComponent<HorizontalScrollSnap> ();
            hss.RemoveAllChildren (out previousChildren);

            //Limpiar los paginadores
            if (paginadorCentral.childCount > 0)
                destroyChildrensTransform (paginadorCentral);
            if (paginadorCursos.childCount > 0)
                destroyChildrensTransform (paginadorCursos);
            if (paginadorEventos.childCount > 0)
                destroyChildrensTransform (paginadorEventos);
            //Habilitar los botones de acuerdo al tipo de ambiente
            switch (ambienteBuscado.getTipoambiente ().getNombre ()) {
                case "Aula":
                    /* Activar los paneles correspondientes */
                    dataBaseDisplay.SetActive (true);
                    tramitesDisplay.SetActive (false);
                    personalDisplay.SetActive (false);
                    //Si esta activo el panel de alumnos
                    if (alumnosDisplay.activeSelf) {
                        navigatorDisplay.SetActive (true);
                        alumnosDisplay.SetActive (false);
                        centralScrollSnap.SetActive(true);
                    }

                    //Añadir la pantalla al scroll
                    hss.AddChild (dataBaseDisplay);
                    //Añadir los campos del paginador
                    GameObject paginadorBaseObject = Instantiate (prefabPaginatorToggle, new Vector3 (0, 0, 0), Quaternion.identity);

                    //Añadirlo al toogleGroup del central
                    Toggle toggleComponentBase = paginadorBaseObject.GetComponent<Toggle> ();
                    toggleComponentBase.group = paginadorCentral.GetComponent<ToggleGroup> ();
                    toggleComponentBase.isOn = true;

                    PaginatorToggle togglePaginadorBase = paginadorBaseObject.GetComponent<PaginatorToggle> ();
                    togglePaginadorBase.Setup ("Inicio", 0, centralScrollSnap.GetComponent<HorizontalScrollSnap> ());

                    paginadorBaseObject.transform.SetParent (paginadorCentral);

                    //Cargar los logos
                    iconoDerecha.GetComponent<UnityEngine.UI.Image> ().sprite = spriteAula;
                    iconoIzquierda.GetComponent<UnityEngine.UI.Image> ().sprite = spriteAula;

                    //Cargar cursos
                    if (ambienteBuscado.getEsusadocursos ().Count > 0) {
                        cursosDisplay.SetActive (true);
                        //Añadir la pantalla al scroll
                        hss.AddChild (cursosDisplay);
                        //Añadir los campos del paginador
                        GameObject paginadorCursosObject = Instantiate (prefabPaginatorToggle, new Vector3 (0, 0, 0), Quaternion.identity);

                        //Añadirlo al toogleGroup del central
                        Toggle toggleComponentCursos = paginadorCursosObject.GetComponent<Toggle> ();
                        toggleComponentCursos.group = paginadorCentral.GetComponent<ToggleGroup> ();

                        PaginatorToggle togglePaginadorCursos = paginadorCursosObject.GetComponent<PaginatorToggle> ();
                        togglePaginadorCursos.Setup ("Cursos", 1, centralScrollSnap.GetComponent<HorizontalScrollSnap> ());

                        paginadorCursosObject.transform.SetParent (paginadorCentral);
                        //Cargar los demas valores
                        cargarCursos ();
                    } else {
                        cursosDisplay.SetActive (false);
                    }

                    //Carga eventos
                    if (ambienteBuscado.getEsUsadoEventos ().Count > 0) {

                        eventosDisplay.SetActive (true);
                        //Añadir la pantalla al scroll
                        hss.AddChild (eventosDisplay);
                        //Añadir los campos del paginador
                        GameObject paginadorCursosObject = Instantiate (prefabPaginatorToggle, new Vector3 (0, 0, 0), Quaternion.identity);

                        //Añadirlo al toogleGroup del central
                        Toggle toggleComponentEventos = paginadorCursosObject.GetComponent<Toggle> ();
                        toggleComponentEventos.group = paginadorCentral.GetComponent<ToggleGroup> ();

                        PaginatorToggle togglePaginadorEventos = paginadorCursosObject.GetComponent<PaginatorToggle> ();
                        if (ambienteBuscado.getEsusadocursos ().Count > 0)
                            togglePaginadorEventos.Setup ("Eventos", 2, centralScrollSnap.GetComponent<HorizontalScrollSnap> ());
                        else
                            togglePaginadorEventos.Setup ("Eventos", 1, centralScrollSnap.GetComponent<HorizontalScrollSnap> ());

                        paginadorCursosObject.transform.SetParent (paginadorCentral);
                        cargarEventos ();
                    } else {
                        eventosDisplay.SetActive (false);
                    }

                    break;
                case "Oficina Administrativa":
                    /* Activar los paneles correspondientes */
                    dataBaseDisplay.SetActive (true);
                    cursosDisplay.SetActive (false);
                    //Si esta activo el panel de alumnos
                    if (alumnosDisplay.activeSelf) {
                        navigatorDisplay.SetActive (true);
                        alumnosDisplay.SetActive (false);
                        centralScrollSnap.SetActive(true);
                    }

                    //Añadir la pantalla al scroll
                    hss.AddChild (dataBaseDisplay);
                    //Añadir los campos del paginador
                    GameObject paginadorBaseOficinaObject = Instantiate (prefabPaginatorToggle, new Vector3 (0, 0, 0), Quaternion.identity);

                    //Añadirlo al toogleGroup del central
                    Toggle toggleComponentBaseOficina = paginadorBaseOficinaObject.GetComponent<Toggle> ();
                    toggleComponentBaseOficina.group = paginadorCentral.GetComponent<ToggleGroup> ();
                    toggleComponentBaseOficina.isOn = true;

                    PaginatorToggle togglePaginadorBaseOficina = paginadorBaseOficinaObject.GetComponent<PaginatorToggle> ();
                    togglePaginadorBaseOficina.Setup ("Inicio", 0, centralScrollSnap.GetComponent<HorizontalScrollSnap> ());

                    paginadorBaseOficinaObject.transform.SetParent (paginadorCentral);

                    //Cargar los logos
                    iconoDerecha.GetComponent<UnityEngine.UI.Image> ().sprite = spriteOficina;
                    iconoIzquierda.GetComponent<UnityEngine.UI.Image> ().sprite = spriteOficina;

                    //Cargar Tramites
                    if (ambienteBuscado.getRealizatramites ().Count > 0) {
                        tramitesDisplay.SetActive (true);
                        //Añadir la pantalla al scroll
                        hss.AddChild (tramitesDisplay);
                        //Añadir los campos del paginador
                        GameObject paginadorTramitesObject = Instantiate (prefabPaginatorToggle, new Vector3 (0, 0, 0), Quaternion.identity);

                        //Añadirlo al toogleGroup del central
                        Toggle toggleComponentTramites = paginadorTramitesObject.GetComponent<Toggle> ();
                        toggleComponentTramites.group = paginadorCentral.GetComponent<ToggleGroup> ();

                        PaginatorToggle togglePaginadorTramites = paginadorTramitesObject.GetComponent<PaginatorToggle> ();
                        togglePaginadorTramites.Setup ("Tramites", 1, centralScrollSnap.GetComponent<HorizontalScrollSnap> ());

                        paginadorTramitesObject.transform.SetParent (paginadorCentral);

                        cargarTramites ();
                    } else {
                        tramitesDisplay.SetActive (false);
                    }

                    //Cargar Personal
                    if (ambienteBuscado.getGestionapersonal ().Count > 0) {
                        personalDisplay.SetActive (true);
                        //Añadir la pantalla al scroll
                        hss.AddChild (personalDisplay);
                        //Añadir los campos del paginador
                        GameObject paginadorPersonalObject = Instantiate (prefabPaginatorToggle, new Vector3 (0, 0, 0), Quaternion.identity);

                        //Añadirlo al toogleGroup del central
                        Toggle toggleComponentPersonal = paginadorPersonalObject.GetComponent<Toggle> ();
                        toggleComponentPersonal.group = paginadorCentral.GetComponent<ToggleGroup> ();

                        PaginatorToggle togglePaginadorPersonal = paginadorPersonalObject.GetComponent<PaginatorToggle> ();
                        if (ambienteBuscado.getRealizatramites ().Count > 0)
                            togglePaginadorPersonal.Setup ("Personal", 2, centralScrollSnap.GetComponent<HorizontalScrollSnap> ());
                        else
                            togglePaginadorPersonal.Setup ("Personal", 1, centralScrollSnap.GetComponent<HorizontalScrollSnap> ());

                        paginadorPersonalObject.transform.SetParent (paginadorCentral);
                        //Cargar los demas valores
                        cargarPersonal ();
                    } else {
                        personalDisplay.SetActive (false);
                    }
                    break;
                case "Auditorio":
                    /* Activar los paneles correspondientes */
                    dataBaseDisplay.SetActive (true);
                    personalDisplay.SetActive (false);
                    cursosDisplay.SetActive (false);
                    //Si esta activo el panel de alumnos
                    if (alumnosDisplay.activeSelf) {
                        navigatorDisplay.SetActive (true);
                        alumnosDisplay.SetActive (false);
                        centralScrollSnap.SetActive(true);
                    }
                    //Añadir la pantalla al scroll
                    hss.AddChild (dataBaseDisplay);
                    //Añadir los campos del paginador
                    GameObject paginadorBaseEventoObject = Instantiate (prefabPaginatorToggle, new Vector3 (0, 0, 0), Quaternion.identity);

                    //Añadirlo al toogleGroup del central
                    Toggle toggleComponentBaseEvento = paginadorBaseEventoObject.GetComponent<Toggle> ();
                    toggleComponentBaseEvento.group = paginadorCentral.GetComponent<ToggleGroup> ();
                    toggleComponentBaseEvento.isOn = true;

                    PaginatorToggle togglePaginadorBaseEvento = paginadorBaseEventoObject.GetComponent<PaginatorToggle> ();
                    togglePaginadorBaseEvento.Setup ("Inicio", 0, centralScrollSnap.GetComponent<HorizontalScrollSnap> ());

                    paginadorBaseEventoObject.transform.SetParent (paginadorCentral);

                    //Cargar los logos
                    iconoDerecha.GetComponent<UnityEngine.UI.Image> ().sprite = spriteAuditorio;
                    iconoIzquierda.GetComponent<UnityEngine.UI.Image> ().sprite = spriteAuditorio;

                    //Cargar Tramites
                    if (ambienteBuscado.getRealizatramites ().Count > 0) {
                        tramitesDisplay.SetActive (true);
                        eventosDisplay.SetActive (true);
                        //Añadir la pantalla al scroll
                        hss.AddChild (tramitesDisplay);
                        //Añadir los campos del paginador
                        GameObject paginadorTramitesObject = Instantiate (prefabPaginatorToggle, new Vector3 (0, 0, 0), Quaternion.identity);

                        //Añadirlo al toogleGroup del central
                        Toggle toggleComponentTramites = paginadorTramitesObject.GetComponent<Toggle> ();
                        toggleComponentTramites.group = paginadorCentral.GetComponent<ToggleGroup> ();

                        PaginatorToggle togglePaginadorTramites = paginadorTramitesObject.GetComponent<PaginatorToggle> ();
                        togglePaginadorTramites.Setup ("Tramites", 1, centralScrollSnap.GetComponent<HorizontalScrollSnap> ());

                        paginadorTramitesObject.transform.SetParent (paginadorCentral);
                        cargarTramites ();
                    } else {
                        tramitesDisplay.SetActive (false);
                    }

                    //Cargar Eventos
                    if (ambienteBuscado.getEsUsadoEventos ().Count > 0) {

                        eventosDisplay.SetActive (true);
                        if (cargarEventos ()) {
                            //Añadir la pantalla al scroll
                            hss.AddChild (eventosDisplay);
                            //Añadir los campos del paginador
                            GameObject paginadorEventosObject = Instantiate (prefabPaginatorToggle, new Vector3 (0, 0, 0), Quaternion.identity);

                            //Añadirlo al toogleGroup del central
                            Toggle toggleComponentEventos = paginadorEventosObject.GetComponent<Toggle> ();
                            toggleComponentEventos.group = paginadorCentral.GetComponent<ToggleGroup> ();

                            PaginatorToggle togglePaginadorEventos = paginadorEventosObject.GetComponent<PaginatorToggle> ();
                            if (ambienteBuscado.getRealizatramites ().Count > 0)
                                togglePaginadorEventos.Setup ("Eventos", 2, centralScrollSnap.GetComponent<HorizontalScrollSnap> ());
                            else
                                togglePaginadorEventos.Setup ("Eventos", 1, centralScrollSnap.GetComponent<HorizontalScrollSnap> ());

                            paginadorEventosObject.transform.SetParent (paginadorCentral);
                        } else {
                            eventosDisplay.SetActive (false);
                        }
                    } else {
                        eventosDisplay.SetActive (false);
                    }
                    break;
            }
            btnLimpiarPantalla.SetActive (true);
        }
    }
    #endregion

    #region Button_Methods
    //Cargar la pantalla de cursos
    public void cargarCursos () {
        List<AmbienteCurso> ambientesCurso = ambienteBuscado.getEsusadocursos ();
        //Ordenarlos    
        ambientesCurso.Sort ((x, y) => x.getCurso ().getNombre ().CompareTo (y.getCurso ().getNombre ()));

        //Obtener el objeto
        VerticalScrollSnap vss = scrollSnapCursos.GetComponent<VerticalScrollSnap> ();

        //Limpiar los hijos
        cleanVerticalScrollSnap (vss);

        //Llenar el object pool
        for (int i = 0; i < ambientesCurso.Count; i++) {
            AmbienteCurso ambienteCursoActual = ambientesCurso[i];
            //Verificar que no exista el valor previamente
            long id = ambienteCursoActual.getId ();
            //Compararlo contra la lista
            bool existe = false;
            if (cursosPanelList.Count > 0) cursosPanelList.Find (x => x.GetComponent<CursoPanel> ().getId () == id);
            //Si no existe añadirlo a la pantalla
            if (!existe) {
                //Objeto con datos
                GameObject cursoPanelObject = cursosPanelPool.GetObject ();
                cursosPanelList.Add (cursoPanelObject);
                CursoPanel cursoPanel = cursoPanelObject.GetComponent<CursoPanel> ();
                cursoPanel.Setup (ambienteCursoActual.getCurso (), ambienteCursoActual.getHorarios (), alumnosPanelPool, alumnosPanelParent, centralScrollSnap, alumnosDisplay, navigatorDisplay);

                vss.AddChild (cursoPanelObject);

                //Toogle del paginador
                GameObject togglePaginadorObject = Instantiate (prefabPaginatorToggle, new Vector3 (0, 0, 0), Quaternion.identity);

                //Añadirlo al toogleGroup del central
                Toggle toggleComponent = togglePaginadorObject.GetComponent<Toggle> ();
                toggleComponent.group = paginadorCursos.GetComponent<ToggleGroup> ();

                //Si es el primero que este encendido
                if (ToggleGroupExtensions.Count (toggleComponent.group) == 1) {
                    toggleComponent.isOn = true;
                }

                PaginatorToggle togglePaginador = togglePaginadorObject.GetComponent<PaginatorToggle> ();
                togglePaginador.SetupCurso (ambienteCursoActual.getCurso (), i, vss);

                togglePaginadorObject.transform.SetParent (paginadorCursos);
            }
        }
    }

    //Cargar la pantalla de tramites
    public void cargarTramites () {
        List<TramiteAdministrativo> tramiteAdministrativos = ambienteBuscado.getRealizatramites ();

        //Ordenarlos    
        tramiteAdministrativos.Sort ((x, y) => x.getCodigo ().CompareTo (y.getCodigo ()));

        //Llenar el object pool
        for (int i = 0; i < tramiteAdministrativos.Count; i++) {
            TramiteAdministrativo tramiteAdministrativoActual = tramiteAdministrativos[i];

            //Verificar que el valor exista previamente
            string codigo = tramiteAdministrativoActual.getCodigo ();
            string nombre = tramiteAdministrativoActual.getNombre ();
            string descripcion = tramiteAdministrativoActual.getDescripcion ();
            long id = tramiteAdministrativoActual.getId ();

            //Compararlo contra la lista
            bool existe = tramitesPanelList.Find (x => x.GetComponent<TramiteAdministrativoPanel> ().getId () == id);
            //Si no existe añadirlo a la pantalla
            if (!existe) {
                GameObject tramitePanelObject = tramitesPanelPool.GetObject ();
                tramitesPanelList.Add (tramitePanelObject);

                TramiteAdministrativoPanel tramiteAdministrativoPanel = tramitePanelObject.GetComponent<TramiteAdministrativoPanel> ();
                tramiteAdministrativoPanel.Setup (id, codigo, nombre, descripcion);

                tramitePanelObject.transform.SetParent (panelParentTramites);
            }
        }
    }

    //Cargar la pantalla de personal
    public void cargarPersonal () {
        List<PersonalAdministrativo> personalAdministrativos = ambienteBuscado.getGestionapersonal ();

        //Ordenarlos    
        personalAdministrativos.Sort ((x, y) => x.getAppaterno ().CompareTo (y.getAppaterno ()));

        //Llenar el object pool
        for (int i = 0; i < personalAdministrativos.Count; i++) {
            PersonalAdministrativo personalAdministrativoActual = personalAdministrativos[i];

            //Verificar que no exista el valor previamente
            string nombreCompleto = personalAdministrativoActual.getAppaterno () + " " + personalAdministrativoActual.getApmaterno () + " " + personalAdministrativoActual.getNombre ();
            string cargo = personalAdministrativoActual.getCargo ().getNombre ();
            long id = personalAdministrativoActual.getId ();
            //Compararlo contra la lista
            bool existe = personalPanelList.Find (x => x.GetComponent<PersonalPanel> ().getId () == id);
            //Si no existe añadirlo a la pantalla
            if (!existe) {
                GameObject personalPanelObject = personalPanelPool.GetObject ();
                personalPanelList.Add (personalPanelObject);

                PersonalPanel personalPanel = personalPanelObject.GetComponent<PersonalPanel> ();
                personalPanel.Setup (id, nombreCompleto, cargo);

                personalPanelObject.transform.SetParent (panelParentPersonal);
            }
        }
    }

    //Cargar la pantalla de eventos
    public bool cargarEventos () {
        List<EventoAmbiente> eventosAmbiente = ambienteBuscado.getEsUsadoEventos ();
        //Verificar que los eventos existan aun
        List<EventoAmbiente> eventosValidos = new List<EventoAmbiente> ();
        foreach (EventoAmbiente ea in eventosAmbiente) {
            Evento evento = ea.getEvento ();
            //Si ya fue realizado no se añade
            if (!evento.getRealizado ()) {
                eventosValidos.Add (ea);
            }
        }

        //Si hay eventos añadirlos sino limpiar los que habian
        if (eventosValidos.Count > 0) {
            //Ordenarlos    
            eventosValidos.Sort ((x, y) => x.getEvento ().getFecha ().CompareTo (y.getEvento ().getFecha ()));

            //Obtener el objeto
            VerticalScrollSnap vss = scrollSnapEventos.GetComponent<VerticalScrollSnap> ();
            //Limpiar los hijos
            cleanVerticalScrollSnap (vss);
            //Llenar el object pool
            for (int i = 0; i < eventosValidos.Count; i++) {
                EventoAmbiente eventoAmbienteActual = eventosValidos[i];

                //Verificar que no exista el valor previamente
                long id = eventoAmbienteActual.getId ();

                //Compararlo contra la lista
                bool existe = eventosPanelList.Find (x => x.GetComponent<EventoPanel> ().getId () == id);
                //Si no existe añadirlo a la pantalla
                if (!existe) {
                    GameObject eventoPanelObject = eventosPanelPool.GetObject ();
                    eventosPanelList.Add (eventoPanelObject);

                    EventoPanel eventoPanel = eventoPanelObject.GetComponent<EventoPanel> ();
                    eventoPanel.Setup (eventoAmbienteActual.getEvento (), eventoAmbienteActual.getHorarios ());

                    vss.AddChild (eventoPanelObject);

                    //Toogle del paginador
                    GameObject togglePaginadorObject = Instantiate (prefabPaginatorToggle, new Vector3 (0, 0, 0), Quaternion.identity);

                    //Añadirlo al toogleGroup del central
                    Toggle toggleComponent = togglePaginadorObject.GetComponent<Toggle> ();
                    toggleComponent.group = paginadorEventos.GetComponent<ToggleGroup> ();

                    //Si es el primero que este encendido
                    if (ToggleGroupExtensions.Count (toggleComponent.group) == 1) {
                        toggleComponent.isOn = true;
                    }

                    PaginatorToggle togglePaginador = togglePaginadorObject.GetComponent<PaginatorToggle> ();
                    togglePaginador.SetupEvento (eventoAmbienteActual.getEvento (), i, vss);

                    togglePaginadorObject.transform.SetParent (paginadorEventos);
                }
            }
            return true;
        } else {
            return false;
        }
    }

    public void regresarCursos () {
        alumnosDisplay.SetActive (false);
        navigatorDisplay.SetActive (true);
        centralScrollSnap.SetActive (true);
        for (int i = 0; i < cursosPanelList.Count; i++) {
            CursoPanel cursoPanel = cursosPanelList[i].GetComponent<CursoPanel> ();
            cursoPanel.removeAlumnoPoolObjects ();
        }
    }
    #endregion

    #region Clean_Methods
    //Limpiar los pools
    private void cleanPools () {
        removePoolObjects (cursosPanelList, cursosPanelPool, "curso");
        removePoolObjects (tramitesPanelList, tramitesPanelPool, "tramites");
        removePoolObjects (personalPanelList, personalPanelPool, "personal");
        removePoolObjects (eventosPanelList, eventosPanelPool, "eventos");
    }

    private void destroyChildrensTransform (Transform tr) {
        foreach (Transform child in tr) {
            GameObject.Destroy (child.gameObject);
        }
    }

    public void clearTrackableName () {
        this.lastMarkerName = null;
    }
    //Remover los hijos de los vertical scroll snao
    private void cleanVerticalScrollSnap (VerticalScrollSnap vss) {
        if (vss.ChildObjects.Length > 0) {
            cleanPools ();
            GameObject lastChild;
            //Limpiar todos los valores
            while (vss.ChildObjects.Length > 0) {
                vss.RemoveChild (0, out lastChild);
            }
        }
    }

    //remover los pools
    private void removePoolObjects (List<GameObject> gameObjects, SimpleObjectPool sop, string objectType) {
        switch (objectType) {
            case "curso":
                while (gameObjects.Count > 0) {
                    CursoPanel cursoPanel = gameObjects[0].GetComponent<CursoPanel> ();
                    sop.ReturnObject (gameObjects[0]);
                    gameObjects.RemoveAt (0);
                    cursoPanel.removePoolObjects ();
                }
                break;
            case "eventos":
                while (gameObjects.Count > 0) {
                    EventoPanel eventoPanel = gameObjects[0].GetComponent<EventoPanel> ();
                    sop.ReturnObject (gameObjects[0]);
                    gameObjects.RemoveAt (0);
                    eventoPanel.removePoolObjects ();
                }
                break;
            default:
                while (gameObjects.Count > 0) {
                    sop.ReturnObject (gameObjects[0]);
                    gameObjects.RemoveAt (0);
                }
                break;
        }
    }
    #endregion

    #region API_Calls
    async Task<Ambiente> GetAmbienteByIDAsync (long id) {
        Ambiente ambiente = null;
        HttpResponseMessage response = await httpClient.GetAsync (urlBase + "/ambientes/" + id);
        if (response.IsSuccessStatusCode) {
            ambiente = await response.Content.ReadAsAsync<Ambiente> ();
        }
        return ambiente;
    }

    async Task<Ambiente> GetAmbienteByIDVuforiaAsync (string id) {
        Ambiente ambiente = null;
        HttpResponseMessage response = await httpClient.GetAsync (urlBase + "/ambientes/idVuforia/" + id);
        if (response.IsSuccessStatusCode) {
            ambiente = await response.Content.ReadAsAsync<Ambiente> ();
        }
        return ambiente;
    }
    #endregion
}