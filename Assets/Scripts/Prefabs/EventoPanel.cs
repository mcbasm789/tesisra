﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class EventoPanel : MonoBehaviour {
    #region Public_Variables
    public Text eventoText;
    public Text aforoText;
    public Text expositorText;
    public Text fechaText;
    public SimpleObjectPool horarioPool;
    public Transform horarioPanelParent;

    #endregion

    #region Private Variables
    //Id para las validaciones
    private long id;
    private GameObject eventosDisplay;
    private Evento eventoActual;
    private List<Horario> horariosDisponibles;
    private List<GameObject> horarioPanelObjectList = new List<GameObject> ();
    #endregion

    // Start is called before the first frame update
    void Start () {

    }

    // Update is called once per frame
    void Update () {
        //Ponerlo a la posicion correcta si es necesario
        float angleX = gameObject.transform.rotation.eulerAngles.x;
        float angleY = gameObject.transform.rotation.eulerAngles.y;
        float angleZ = gameObject.transform.rotation.eulerAngles.z;
        if (angleX != 0 || angleY != 0 || angleZ != 0)
            gameObject.transform.rotation = Quaternion.identity;
    }

    #region Init_Methods
    //Inicializar el componente
    public void Setup (Evento evento, List<Horario> horarios) {
        eventoActual = evento;
        horariosDisponibles = horarios;
        //Inicializar los campos
        eventoText.text = evento.getNombre ().ToString ();
        expositorText.text = evento.getExpositor ();
        aforoText.text = evento.getAforo ().ToString ();
        fechaText.text = evento.getFecha ().ToString ("dd/MM/yyyy");
        setId (evento.getId ());

        //Ordenarlos    
        horariosDisponibles.Sort ((x, y) => x.getNroDia ().CompareTo (y.getNroDia ()));

        for (int i = 0; i < horariosDisponibles.Count; i++) {
            Horario horarioActual = horariosDisponibles[i];

            //Verificar que no exista el valor previamente
            long id = horarioActual.getId ();

            //Compararlo contra la lista
            bool existe = horarioPanelObjectList.Find (x => x.GetComponent<HorarioPanel> ().getId () == id);

            if (!existe) {
                GameObject horarioPanelObject = horarioPool.GetObject ();
                horarioPanelObjectList.Add (horarioPanelObject);
                horarioPanelObject.transform.SetParent (horarioPanelParent);

                HorarioPanel horarioPanel = horarioPanelObject.GetComponent<HorarioPanel> ();
                horarioPanel.Setup (horarioActual.getId (), horarioActual.getDia () + " " + horarioActual.getHorainicio () + "-" + horarioActual.getHorafin ());
            }
        }
    }
    #endregion

    #region Cleaning_Methods
    public void removePoolObjects () {
        while (horarioPanelObjectList.Count > 0) {
            horarioPool.ReturnObject (horarioPanelObjectList[0]);
            horarioPanelObjectList.RemoveAt (0);
        }
    }
    #endregion

    #region GET_SET
    public long getId () {
        return this.id;
    }

    public void setId (long id) {
        this.id = id;
    }
    #endregion
}