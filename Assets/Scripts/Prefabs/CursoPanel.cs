﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CursoPanel : MonoBehaviour {
    #region Public_Variables
    public Text cursoText;
    public SimpleObjectPool horarioPool;
    public Transform horarioPanelParent;
    public Text docenteText;
    public Text escuelaProfesionalText;
    //Display del boton ver alumnos
    public GameObject btnVerAlumnos;
    #endregion

    #region Private Variables
    //Id para las validaciones
    private long id;
    private SimpleObjectPool alumnosPanelPool;
    private Transform alumnosPanelParent;
    private GameObject centralPanel;
    private GameObject alumnosDisplay;
    private GameObject navigatorDisplay;
    private Curso cursoActual;
    private List<Horario> horariosDisponibles;
    private List<GameObject> horarioPanelObjectList = new List<GameObject> ();
    private List<Alumno> alumnosDisponibles;
    private List<GameObject> alumnosPanelList = new List<GameObject> ();
    #endregion

    // Start is called before the first frame update
    void Start () {

    }

    // Update is called once per frame
    void Update () {
        //Ponerlo a la posicion correcta si es necesario
        float angleX = gameObject.transform.rotation.eulerAngles.x;
        float angleY = gameObject.transform.rotation.eulerAngles.y;
        float angleZ = gameObject.transform.rotation.eulerAngles.z;
        if (angleX != 0 || angleY != 0 || angleZ != 0)
            gameObject.transform.rotation = Quaternion.identity;
    }

    #region Init_Methods
    //Inicializar el componente
    public void Setup (Curso curso, List<Horario> horarios, SimpleObjectPool alumnosPool, Transform alumnosParent, GameObject centralPanel, GameObject alumnosDisplay, GameObject navigatorDisplay) {
        //Inicializar las variables para el cambio de pantalla
        this.alumnosPanelPool = alumnosPool;
        this.alumnosPanelParent = alumnosParent;
        this.centralPanel = centralPanel;
        this.alumnosDisplay = alumnosDisplay;
        this.navigatorDisplay = navigatorDisplay;
        setId (curso.getId ());

        cursoActual = curso;
        horariosDisponibles = horarios;
        cursoText.text = (curso.getCodigo () + " " + curso.getNombre ()).ToString ();
        escuelaProfesionalText.text = "Escuela: " + curso.getEscuelaprofesional ().getNombre ();
        //Cargar los docentes
        int contador = 0;
        if (curso.getEsdictadodocentes () != null)
            if (curso.getEsdictadodocentes ().Count > 0)
                foreach (var docente in curso.getEsdictadodocentes ()) {
                    string nombreDocente = docente.getAppaterno () + " " + docente.getApmaterno () + " " + docente.getNombre ();
                    if (contador > 0) {
                        docenteText.text = docenteText.text + "\n" + nombreDocente;
                    } else {
                        docenteText.text = "Docente:" + nombreDocente;
                    }
                    contador++;
                }

        //Verificar si existen alumnos
        List<Alumno> alumnos = cursoActual.getEsllevadoalumnos ();
        if (alumnos.Count > 0) {
            btnVerAlumnos.SetActive (true);
        } else {
            btnVerAlumnos.SetActive (false);
        }

        //Ordenarlos    
        //horariosDisponibles.Sort ((x, y) => x.getNroDia ().CompareTo (y.getNroDia ()));

        for (int i = 0; i < horariosDisponibles.Count; i++) {
            Horario horarioActual = horariosDisponibles[i];

            //Verificar que no exista el valor previamente
            long id = horarioActual.getId ();

            //Compararlo contra la lista
            bool existe = horarioPanelObjectList.Find (x => x.GetComponent<HorarioPanel> ().getId () == id);
            //Si no existe añadirlo a la pantalla
            if (!existe) {
                GameObject horarioPanelObject = horarioPool.GetObject ();
                horarioPanelObjectList.Add (horarioPanelObject);
                horarioPanelObject.transform.SetParent (horarioPanelParent);

                HorarioPanel horarioPanel = horarioPanelObject.GetComponent<HorarioPanel> ();
                horarioPanel.Setup (horarioActual.getId (), horarioActual.getDia () + " " + horarioActual.getHorainicio () + "-" + horarioActual.getHorafin ());
            }
        }
    }
    #endregion

    #region Button_Methods
    public void cargarAlumnos () {
        List<Alumno> alumnos = cursoActual.getEsllevadoalumnos ();
        if (alumnos.Count > 0) {
            //Ordenarlos    
            alumnos.Sort ((x, y) => x.getAppaterno ().CompareTo (y.getAppaterno ()));
            //Mostrar el panel de alumnos
            centralPanel.SetActive (false);
            alumnosDisplay.SetActive (true);
            navigatorDisplay.SetActive (false);
            //Llenar el object pool
            for (int i = 0; i < alumnos.Count; i++) {
                Alumno alumnoActual = alumnos[i];

                //Verificar que no exista el valor previamente
                long id = alumnoActual.getId ();

                //Compararlo contra la lista
                bool existe = alumnosPanelList.Find (x => x.GetComponent<AlumnoPanel> ().getId () == id);
                //Si no existe añadirlo a la pantalla
                if (!existe) {
                    GameObject alumnoPanelObject = alumnosPanelPool.GetObject ();
                    alumnosPanelList.Add (alumnoPanelObject);
                    alumnoPanelObject.transform.SetParent (alumnosPanelParent);

                    AlumnoPanel AlumnoPanel = alumnoPanelObject.GetComponent<AlumnoPanel> ();
                    AlumnoPanel.Setup (alumnoActual.getId (), alumnoActual.getCodigo (), alumnoActual.getAppaterno () + " " + alumnoActual.getApmaterno () + " " + alumnoActual.getNombre ());
                }
            }
        }
    }

    #endregion

    #region Cleaning_Methods
    public void removeAlumnoPoolObjects () {
        while (alumnosPanelList.Count > 0) {
            alumnosPanelPool.ReturnObject (alumnosPanelList[0]);
            alumnosPanelList.RemoveAt (0);
        }
    }

    public void removePoolObjects () {
        while (horarioPanelObjectList.Count > 0) {
            horarioPool.ReturnObject (horarioPanelObjectList[0]);
            horarioPanelObjectList.RemoveAt (0);
        }
    }
    #endregion

    #region GET_SET
    public long getId () {
        return this.id;
    }

    public void setId (long id) {
        this.id = id;
    }
    #endregion

}