﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AlumnoPanel : MonoBehaviour {
    //Id para las validaciones
    private long id;

    //Variables de la interfaz
    public Text codigoText;
    public Text nombreText;

    // Start is called before the first frame update
    void Start () {

    }

    // Update is called once per frame
    void Update () {
        //Ponerlo a la posicion correcta si es necesario
        float angleX = gameObject.transform.rotation.eulerAngles.x;
        float angleY = gameObject.transform.rotation.eulerAngles.y;
        float angleZ = gameObject.transform.rotation.eulerAngles.z;
        if (angleX != 0 || angleY != 0 || angleZ != 0)
            gameObject.transform.rotation = Quaternion.identity;
    }

    //Inicializar el componente
    public void Setup (long id, string codigo, string nombre) {
        setId (id);
        codigoText.text = codigo;
        nombreText.text = nombre;
    }

    public long getId () {
        return this.id;
    }

    public void setId (long id) {
        this.id = id;
    }
}