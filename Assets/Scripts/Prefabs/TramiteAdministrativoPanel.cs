﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TramiteAdministrativoPanel : MonoBehaviour {
    //Variables a mostrar
    public Text codigoText;
    public Text nombreText;
    public Text descripcionText;
    //Id para las validaciones
    private long id;

    // Start is called before the first frame update
    void Start () {

    }

    //Inicializar el componente
    public void Setup (long id, string codigo, string nombre, string descripcion) {
        setId (id);
        codigoText.text = codigo;
        nombreText.text = nombre;
        descripcionText.text = descripcion;
    }

    // Update is called once per frame
    void Update () {
        //Ponerlo a la posicion correcta si es necesario
        float angleX = gameObject.transform.rotation.eulerAngles.x;
        float angleY = gameObject.transform.rotation.eulerAngles.y;
        float angleZ = gameObject.transform.rotation.eulerAngles.z;
        if (angleX != 0 || angleY != 0 || angleZ != 0)
            gameObject.transform.rotation = Quaternion.identity;
    }

    public long getId () {
        return this.id;
    }

    public void setId (long id) {
        this.id = id;
    }
}