﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;

public class PaginatorToggle : MonoBehaviour {
    #region Public_Variables
    public Text label;
    #endregion

    #region Private_Variables
    private int orden = 0;
    private HorizontalScrollSnap hss;
    private VerticalScrollSnap vss;
    private string tipoScroll;
    private bool cambioManual = false;
    #endregion

    // Start is called before the first frame update
    void Start () {

    }

    // Update is called once per frame
    void Update () {
        //Ponerlo a la posicion correcta si es necesario
        float angleX = gameObject.transform.rotation.eulerAngles.x;
        float angleY = gameObject.transform.rotation.eulerAngles.y;
        float angleZ = gameObject.transform.rotation.eulerAngles.z;
        if (angleX != 0 || angleY != 0 || angleZ != 0)
            gameObject.transform.rotation = Quaternion.identity;
    }

    #region Public_Mehtods
    public void SetupCurso (Curso curso, int orden, VerticalScrollSnap vssOriginal) {
        this.label.text = curso.getNombre ();
        this.orden = orden;
        this.vss = vssOriginal;
        tipoScroll = "Vertical";
    }

    public void SetupEvento (Evento evento, int orden, VerticalScrollSnap vssOriginal) {
        this.label.text = evento.getNombre ();
        this.orden = orden;
        this.vss = vssOriginal;
        tipoScroll = "Vertical";
    }

    public void Setup (string nombre, int orden, HorizontalScrollSnap hssOriginal) {
        this.label.text = nombre;
        this.orden = orden;
        this.hss = hssOriginal;
        tipoScroll = "Horizontal";
    }

    //On Value Changed
    public void onValueChanged () {
        cambioManual = true;
        Toggle toggle = gameObject.GetComponent<Toggle> ();
        if (toggle.isOn) {
            switch (tipoScroll) {
                case "Vertical":
                    vss.GoToScreen (orden);
                    break;
                case "Horizontal":
                    hss.GoToScreen (orden);
                    break;
            }
        }
        cambioManual = false;
    }
    #endregion

    #region GET_SET
    public int getOrden () {
        return this.orden;
    }

    public void setOrden (int orden) {
        this.orden = orden;
    }
    #endregion
}