﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;
#if PLATFORM_ANDROID
using UnityEngine.Android;
#endif

public class StartController : MonoBehaviour {
    #region Public_Variables
    //Botones
    public GameObject startHSS;
    GameObject dialog = null;
    #endregion

    #region Private_Variables
    private HorizontalScrollSnap hss;
    #endregion

    // Start is called before the first frame update
    void Start () {
        /* Verificar permisos de camara */
#if PLATFORM_ANDROID
        if (!Permission.HasUserAuthorizedPermission (Permission.Camera)) {
            Permission.RequestUserPermission (Permission.Camera);
            dialog = new GameObject ();
        }
#endif
        this.hss = startHSS.GetComponent<HorizontalScrollSnap> ();
    }

    void OnGUI () {
#if PLATFORM_ANDROID
        if (!Permission.HasUserAuthorizedPermission (Permission.Camera)) {
            dialog.AddComponent<PermissionsRationaleDialog> ();
            return;
        } else if (dialog != null) {
            Destroy (dialog);
        }
#endif

    }

    // Update is called once per frame
    void Update () {

    }

    #region Button_Methods
    // Iniciar la camara
    public void Iniciar () {
        SceneManager.LoadScene ("Main", LoadSceneMode.Single);
    }

    // Ir al Tutorial
    public void IrTutorial () {
        hss.ChangePage (1);
    }

    //Regresar al inicio
    public void RegresarInicio () {
        hss.ChangePage (0);
    }
    #endregion
}